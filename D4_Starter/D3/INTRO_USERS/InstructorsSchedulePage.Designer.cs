﻿namespace INTRO_USERS
{
    partial class InstructorsSchedulePage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dateTimePickerSchedule = new System.Windows.Forms.DateTimePicker();
            this.buttonSaveSchedule = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.comboBoxInstructor = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.labelDayHours = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // dateTimePickerSchedule
            // 
            this.dateTimePickerSchedule.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePickerSchedule.Location = new System.Drawing.Point(97, 34);
            this.dateTimePickerSchedule.Name = "dateTimePickerSchedule";
            this.dateTimePickerSchedule.Size = new System.Drawing.Size(105, 20);
            this.dateTimePickerSchedule.TabIndex = 1;
            this.dateTimePickerSchedule.ValueChanged += new System.EventHandler(this.dateTimePickerSchedule_ValueChanged);
            // 
            // buttonSaveSchedule
            // 
            this.buttonSaveSchedule.Location = new System.Drawing.Point(180, 319);
            this.buttonSaveSchedule.Name = "buttonSaveSchedule";
            this.buttonSaveSchedule.Size = new System.Drawing.Size(103, 32);
            this.buttonSaveSchedule.TabIndex = 4;
            this.buttonSaveSchedule.Text = "Save Schedule";
            this.buttonSaveSchedule.UseVisualStyleBackColor = true;
            this.buttonSaveSchedule.Click += new System.EventHandler(this.buttonSaveSchedule_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(85, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Instructor Name:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 34);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Date:";
            // 
            // comboBoxInstructor
            // 
            this.comboBoxInstructor.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append;
            this.comboBoxInstructor.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBoxInstructor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxInstructor.Enabled = false;
            this.comboBoxInstructor.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.comboBoxInstructor.FormattingEnabled = true;
            this.comboBoxInstructor.Location = new System.Drawing.Point(97, 6);
            this.comboBoxInstructor.Name = "comboBoxInstructor";
            this.comboBoxInstructor.Size = new System.Drawing.Size(105, 21);
            this.comboBoxInstructor.TabIndex = 7;
            this.comboBoxInstructor.SelectedValueChanged += new System.EventHandler(this.comboBoxInstructor_SelectedValueChanged);
            this.comboBoxInstructor.Click += new System.EventHandler(this.comboBoxInstructor_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 63);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(84, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Available Times:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 329);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Total Hours:";
            // 
            // labelDayHours
            // 
            this.labelDayHours.AutoSize = true;
            this.labelDayHours.Location = new System.Drawing.Point(94, 329);
            this.labelDayHours.Name = "labelDayHours";
            this.labelDayHours.Size = new System.Drawing.Size(33, 13);
            this.labelDayHours.TabIndex = 10;
            this.labelDayHours.Text = "hours";
            // 
            // InstructorsSchedulePage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(301, 368);
            this.Controls.Add(this.labelDayHours);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.comboBoxInstructor);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonSaveSchedule);
            this.Controls.Add(this.dateTimePickerSchedule);
            this.Name = "InstructorsSchedulePage";
            this.Text = "InstructorsSchedulePage";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.DateTimePicker dateTimePickerSchedule;
        private System.Windows.Forms.Button buttonSaveSchedule;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox comboBoxInstructor;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label labelDayHours;
    }
}